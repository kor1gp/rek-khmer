local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local widget = require("widget")


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local screenGroup = self.view

	print( "\nsetting: createScene event" )
end



-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local screenGroup = self.view
	
	
	-- Label Title
	local background = display.newImageRect ("images/setting_bg.png", display.contentWidth, display.contentHeight)
	background.x = display.contentWidth/2
	background.y = display.contentHeight/2
	screenGroup:insert(background)
	
	
	-- Button Back Player	
	local onBtnSinglePlayerEvent = function( event )
		if event.phase == "ended" then			
			storyboard.gotoScene( "mainmenu", "fade", 300  )
		end
	end
	local btnSinglePlayer = widget.newButton
	{
	   left = 20,
       top = 20,    
       width = 50,                  
       height = 40,                      
       defaultFile = "images/back_btn.png", 
       overFile = "images/back_btn_over.png",
       
       onEvent = onBtnSinglePlayerEvent
	}
	screenGroup:insert( btnSinglePlayer )
	
	-- Sound Switch button
	local function onSoundSwitchPress( event )
	   local switch = event.target
	   local response = switch.id.." is on: "..tostring( switch.isOn )
	   print( response )
	end
	local soundSwitch = widget.newSwitch
	{
	   left = 120,
	   top = 90,
	   id = "My on/off switch widget",
	   initialSwitchState = true,
	   onRelease = onSoundSwitchPress
	}
	screenGroup:insert(soundSwitch)
	
	-- Sound Music button
	local function onMusicSwitchPress( event )
	   local switch = event.target
	   local response = switch.id.." is on: "..tostring( switch.isOn )
	   print( response )
	end
	local musicSwitch = widget.newSwitch
	{
	   left = 120,
	   top = 145,
	   id = "My on/off switch widget",
	   initialSwitchState = true,
	   onRelease = onMusicSwitchPress
	}
	screenGroup:insert(musicSwitch)
	
	-- fucntion for textfield
	local function textListener( event )
	    if event.phase == "began" then
	
	        -- user begins editing textField
	        event.target.text = ''
	
	    elseif event.phase == "ended" then
			if(event.target.text == '') then
				event.target.text = event.target.hintTextend
	        -- textField/Box loses focus
			end
	    elseif event.phase == "ended" or event.phase == "submitted" then
	
	        -- do something with defaulField's text
	
	    elseif event.phase == "editing" then        
	
	    end
	end

	-- TextField for player 1	
	local textFieldBg1 = display.newImageRect('images/name_box.png',180,30)
	textFieldBg1.x = 200; textFieldBg1.y = 233
		
	local textField1 = native.newTextField( 40, 85, 120, 30)
	textField1.x = 180; textField1.y = 233	
	textField1.size = 1
	textField1.font = native.newFont( "PTSans-Regular", 17 )
	textField1.align = "left"
	textField1.hasBackground = false	
	textField1.hintText = "Sophea"
	textField1.text = textField1.hintText
	textField1:addEventListener("userInput", textListener)
	
	screenGroup:insert(textFieldBg1)
	screenGroup:insert(textField1)
	
	-- add Save button to textField 1
	local onBtnSave1Event = function( event )
		if event.phase == "ended" then			
			--
		end
	end
	local btnSave1 = widget.newButton
	{
	   left = 250,
       top = 222,    
       width = 30,                  
       height = 20,                      
       defaultFile = "images/save_btn.png", 
       overFile = "images/save_btn_over.png",
       
       onEvent = onBtnSave1Event
	}
	screenGroup:insert( btnSave1 )
	
	
	-- TextField for player 2	
	local textFieldBg2 = display.newImageRect('images/name_box.png',180,30)
	textFieldBg2.x = 200; textFieldBg2.y = 278
		
	local textField2 = native.newTextField( 40, 85, 120, 30)
	textField2.x = 180; textField2.y = 278	
	textField2.size = 1
	textField2.font = native.newFont( "PTSans-Regular", 17 )
	textField2.align = "left"
	textField2.hasBackground = false	
	textField2:addEventListener("userInput", textListener)
	
	screenGroup:insert(textFieldBg2)
	screenGroup:insert(textField2)
	
	-- add Save button to textField 2
	local onBtnSave2Event = function( event )
		if event.phase == "ended" then			
			--
		end
	end
	local btnSave2 = widget.newButton
	{
	   left = 250,
       top = 268,    
       width = 30,                  
       height = 20,                      
       defaultFile = "images/save_btn.png", 
       overFile = "images/save_btn_over.png",
       
       onEvent = onBtnSave2Event
	}
	screenGroup:insert( btnSave2 )
	
end



-- Called when scene is about to move offscreen:
function scene:exitScene()

	--print( "mainmenu: exitScene event" )

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
	--print( "((destroying mainmenu's view))" )
end



---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene