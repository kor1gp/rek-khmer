local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local widget = require("widget")


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local screenGroup = self.view

	print( "\nmainmenu: createScene event" )
end



-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local screenGroup = self.view
	
	
	-- Label Title
	local background = display.newImageRect ("images/main_menu.png", display.contentWidth, display.contentHeight)
	background.x = display.contentWidth/2
	background.y = display.contentHeight/2
	screenGroup:insert(background)
	
	
	-- Button Single Player	
	local onBtnSinglePlayerEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnSinglePlayer = widget.newButton
	{
	   left = 10,
       top = 120,    
       width = 160,                  
       height = 40,                      
       defaultFile = "images/single_player.png", 
       overFile = "images/single_player_over.png",
       
       onEvent = onBtnSinglePlayerEvent
	}
	screenGroup:insert( btnSinglePlayer )
	
	-- Button Multi Player	
	local onBtnMultiPlayerEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnMultiPlayer = widget.newButton
	{
	   left = 10,
       top = 180,    
       width = 160,                  
       height = 40,                      
       defaultFile = "images/multi_player.png", 
       overFile = "images/multi_player_over.png",
       
       onEvent = onBtnMultiPlayerEvent
	}
	screenGroup:insert( btnMultiPlayer )
	
	-- Button Wifi Playing	
	local onBtnWifiPlayingEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnWifiPlaying = widget.newButton
	{
	   left = 10,
       top = 240,    
       width = 160,                  
       height = 40,                      
       defaultFile = "images/wifi_playing.png", 
       overFile = "images/wifi_playing_over.png",
       
       onEvent = onBtnWifiPlayingEvent
	}
	screenGroup:insert( btnWifiPlaying )
	
	-- Button Setting	
	local onBtnSettingEvent = function( event )
		if event.phase == "ended" then
			storyboard.gotoScene( "setting", "fade", 300  )
		end
	end
	local btnSetting = widget.newButton
	{
	   left = 10,
       top = 300,    
       width = 160,                  
       height = 40,                      
       defaultFile = "images/setting.png", 
       overFile = "images/setting_over.png",
       
       onEvent = onBtnSettingEvent
	}
	screenGroup:insert( btnSetting )
	
	-- Button How to play	
	local onBtnHowToPlayEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnHowToPlay = widget.newButton
	{
	   left = 10,
       top = 360,    
       width = 160,                  
       height = 40,                      
       defaultFile = "images/howtoplay.png", 
       overFile = "images/howtoplay_over.png",
       
       onEvent = onBtnHowToPlayEvent
	}
	screenGroup:insert( btnHowToPlay )
	
	-- Button Facebook
	local onBtnFacebookEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnFacebook = widget.newButton
	{
	   left = 250,
       top = 360,    
       width = 50,                  
       height = 40,                      
       defaultFile = "images/facebook.png", 
       overFile = "images/facebook_over.png",
       
       onEvent = onBtnFacebookEvent
	}
	screenGroup:insert( btnFacebook )
	
	-- Button Cambodia	
	local onBtnCambodiaEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnCambodia = widget.newButton
	{
	   left = 10,
       top = 448,    
       width = 50,                  
       height = 30,                      
       defaultFile = "images/cambodia_flag.png", 
       --overFile = "images/back_over.png",
       
       onEvent = onBtnCambodiaEvent
	}
	screenGroup:insert( btnCambodia )
	
	-- Button English	
	local onBtnEnglishEvent = function( event )
		if event.phase == "ended" then
			--storyboard.gotoScene( "characters", "fade", 300  )
		end
	end
	local btnEnglish = widget.newButton
	{
	   left = 260,
       top = 448,    
       width = 50,                  
       height = 30,                      
       defaultFile = "images/english_flag.png", 
       --overFile = "images/back_over.png",
       
       onEvent = onBtnEnglishEvent
	}
	screenGroup:insert( btnEnglish )
end



-- Called when scene is about to move offscreen:
function scene:exitScene()

	--print( "mainmenu: exitScene event" )

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
	--print( "((destroying mainmenu's view))" )
end



---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene